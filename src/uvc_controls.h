
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <linux/videodev2.h>
#include <linux/uvcvideo.h>
#include <linux/usb/video.h>
#include <errno.h>

#define CONTROL_LASER_POWER 1
#define CONTROL_IVCAM_SETTING 2
#define CONTROL_MOTION_RANGE_TRADEOFF 3

#ifdef __cplusplus
extern "C" {
#endif

  void uvc_control_set(int fd, uint8_t control, int32_t setting);
  void uvc_control_get(int fd, uint8_t control);
#ifdef __cplusplus
}
#endif
