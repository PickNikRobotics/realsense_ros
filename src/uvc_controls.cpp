
#include "uvc_controls.h"

void uvc_control_set(int fd, uint8_t control, int32_t setting)
{
  __u8 selector = (__u8)control;
//    __u8 selector = 1;
    __u8 unit = 6;
    __u16 size = 1;
    __u8 value = (__u8)setting;
    int retVal;
//    __u8 * data = (__u8 *)&value;
    struct uvc_xu_control_query xquery;
    memset(&xquery, 0, sizeof(xquery));
    xquery.query = UVC_SET_CUR;
    xquery.size = size;
    xquery.selector = selector;
    xquery.unit = unit;
    xquery.data = (__u8 *)&value;
    retVal = ioctl(fd,UVCIOC_CTRL_QUERY,&xquery);
    if(-1 == retVal){
        perror("UVC_SET_CUR");
    }
    printf("Set control [0x%x] to: [0x%x]\n", control, value);
}

void uvc_control_get(int fd, uint8_t control)
{
  __u8 selector = (__u8)control;
//    __u8 selector = 1;
    __u8 unit = 6;
    __u16 size = 1;
    __u8 value;
    int retVal;
    
    struct uvc_xu_control_query xquery;
    memset(&xquery, 0, sizeof(xquery));
    xquery.query = UVC_GET_CUR;
    xquery.size = size;
    xquery.selector = selector;
    xquery.unit = unit;
    xquery.data = (__u8 *)&value;
    retVal = ioctl(fd,UVCIOC_CTRL_QUERY,&xquery);
    if(-1 == retVal){
        perror("UVC_GET_CUR");
	printf("ioctl() returns: %d, %s \n", errno, strerror(errno));
    }

    printf("Got control [0x%x]: 0x%x\n", control, value);
}
